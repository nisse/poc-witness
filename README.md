# Documentation for setup of sigsum-py witness on poc.sigsum.org

## Witness user

There's a user account "poc-witness" on poc.sigsum.org, with home
directory /home/poc-witness.

## Installation

To install witness and dependencies, the following commands are needed:

```sh
	go install sigsum.org/sigsum-go/cmd/...@v0.9.1
export ${HOME}/go/bin:$PATH
mkdir config && sigsum-key gen -o config/witness-key
```

## Public key

The witness' hex public key is
`1c25f8a44c635457e2e391d1efbca7d4c2951a0aef06225a881e46b98962ac6c`,
keyhash `1c997261f16e6e81d13f420900a2542a4b6a049c2d996324ee5d82a90ca3360c`.

## Starting the witness

To start via systemd, symlink the witness.service file to
`${HOME}/.config/systemd/user/witness.service`, make sure that the
service is enabled, and the user has "lingering" enabled. To be able
to contact systemd, you may need export
`XDG_RUNTIME_DIR=/run/user/$(id -u)`.

## Checking if witness is up

`wget -q -O -
http://poc.sigsum.org:3812/get-tree-size/c9e525b98f412ede185ff2ac5abf70920a2e63a6ae31c88b1138b85de328706b`

(will stop working once support for old witness protocol is removed).

## TODO:

 * Decide and setup logrotation, probably best to let systemd start
   the witness and collect the log messages.

## Migration

On 2024-09-30, witness was migrated from sigsum-py to sigsum-go's
sigsum-witness, to add support for new witness protocol. Last tree
head signed by old witness implementation:

    size=30539
    root_hash=074ea522c6607c5b1aeed1070f9578ca0212d5c1283e751179d49c3650b83bae
    signature=e20a19af64fd482039df96b9b34b00be3d3a72bd3ee1b2a5e6b9a1db1131772706d1fb6931defcc9\
    966ea4fb55eb1cc4fa114d128e3d0cfbc196e334fa1a9d05

A cosignature was added by running

    $ go run ./migrate-state.go ../../config/log-key.pub ../../config/witness-key < ../../config/signed-tree-head.old >../../config/cosigned-tree-head

Resulting tree head:

    size=30539
    root_hash=074ea522c6607c5b1aeed1070f9578ca0212d5c1283e751179d49c3650b83bae
    signature=e20a19af64fd482039df96b9b34b00be3d3a72bd3ee1b2a5e6b9a1db1131772706d1fb6931defcc9966ea4fb55eb1cc4fa114d128e3d0cfbc196e334fa1a9d05
    cosignature=1c997261f16e6e81d13f420900a2542a4b6a049c2d996324ee5d82a90ca3360c 1727679646 c294a8b6d2954167e1b82ef3f1cf4dadc57dc72241a8e0c4aedc2b25e911fa281a8b0b7e49cd9c494ff382adf0534924e001e12ba69e0dba87a1450bf342ea04
