package main

import (
	"fmt"
	"io"
	"net/http"
	"os"
)

func main() {
	mux := http.NewServeMux()
	mux.HandleFunc("POST /", func(w http.ResponseWriter, r *http.Request) {
		fmt.Printf("url: %q\n", r.URL)
		io.Copy(os.Stdout, r.Body)
		http.Error(w, "not handled", http.StatusServiceUnavailable)
	})
	http.ListenAndServe(":3812", mux)
}
