#! /bin/bash

set -e

PATH=${HOME}/go/bin:$PATH
export PATH

# Likely breaks with arguments including whitespace,
# but intended mainly for --bootstrap-log
WITNESS_ARGS="$*"

sigsum-witness -k config/witness-key --log-key config/log-key.pub \
  --state-file config/cosigned-tree-head ${WITNESS_ARGS} :3812
