package main

import (
	"log"
	"os"
	"time"

	"sigsum.org/sigsum-go/pkg/crypto"
	"sigsum.org/sigsum-go/pkg/key"
	"sigsum.org/sigsum-go/pkg/types"
)

func main() {
	if len(os.Args) != 3 {
		log.Fatal("Usage: migrate-state LOG-PUBLIC-KEY PRIVATE-KEY-FILE < STH > CTH")
	}
	logPub, err := key.ReadPublicKeyFile(os.Args[1])
	if err != nil {
		log.Fatal(err)
	}
	signer, err := key.ReadPrivateKeyFile(os.Args[2])
	if err != nil {
		log.Fatal(err)
	}
	var sth types.SignedTreeHead
	if err := sth.FromASCII(os.Stdin); err != nil {
		log.Fatal(err)
	}
	if !sth.Verify(&logPub) {
		log.Fatalf("signature not valid")
	}
	logKeyHash := crypto.HashBytes(logPub[:])
	cs, err := sth.Cosign(signer, &logKeyHash, uint64(time.Now().Unix()))
	if err != nil {
		log.Fatal(err)
	}
	pub := signer.Public()
	cth := types.CosignedTreeHead{
		SignedTreeHead: sth,
		Cosignatures: map[crypto.Hash]types.Cosignature{
			crypto.HashBytes(pub[:]): cs,
		},
	}
	if err := cth.ToASCII(os.Stdout); err != nil {
		log.Fatal(err)
	}
}
